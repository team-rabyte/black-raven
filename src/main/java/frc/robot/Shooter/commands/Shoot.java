/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.Shooter.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.Robot;

public class Shoot extends CommandBase {
  /**
   * Creates a new Shoot.
   */
    //Shooter shooter = new Shooter();
    private boolean isFinished = false;
  public Shoot() {
    //se addRequirements() here to declare subsystem dependencies.
    addRequirements(Robot.shooter);

  }


  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    Robot.shooter.SetMotors(88);
   
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
      Robot.shooter.SetMotors(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return isFinished;
  }

  public void stop()
  {
    isFinished=true;
  }
}
