
package frc.robot.subsystems;

import com.analog.adis16448.frc.ADIS16448_IMU;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.TalonSRXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveTrain extends SubsystemBase {
  private ADIS16448_IMU gyro;

  private TalonSRX leftMotorMaster;
  private VictorSPX leftMotorSlave;
  private TalonSRX rightMotorMaster;
  private VictorSPX rightMotorSlave;

  public DriveTrain() {
    leftMotorMaster = new TalonSRX(Constants.DRIVE_BASE_LEFT_MOTOR_MASTER);
    leftMotorSlave = new VictorSPX(Constants.DRIVE_BASE_LEFT_MOTOR_SLAVE);
    rightMotorMaster = new TalonSRX(Constants.DRIVE_BASE_RIGHT_MASTER);
    rightMotorSlave = new VictorSPX(Constants.DRIVE_BASE_RIGHT_MOTOR_SLAVE);

    gyro = new ADIS16448_IMU();

    leftMotorSlave.follow(leftMotorMaster);
    rightMotorSlave.follow(rightMotorMaster);

    leftMotorMaster.setInverted(true);
    leftMotorSlave.setInverted(true);
    rightMotorMaster.setInverted(false);
    rightMotorSlave.setInverted(false);

    leftMotorMaster.configSelectedFeedbackSensor(TalonSRXFeedbackDevice.QuadEncoder,0,10);
    leftMotorMaster.setSensorPhase(true);
    leftMotorMaster.setSelectedSensorPosition(0,0,10);
    rightMotorMaster.configSelectedFeedbackSensor(TalonSRXFeedbackDevice.QuadEncoder,0,10);
    rightMotorMaster.setSensorPhase(false);
    rightMotorMaster.setSelectedSensorPosition(0,0,10);
  }

  public void setMotors(double left_speed, double right_speed) {
    
    left_speed = Math.min(left_speed, 1);     left_speed = Math.max(left_speed, -1);
    right_speed = Math.min(right_speed, 1);     right_speed = Math.max(right_speed, -1);

    leftMotorMaster.set(ControlMode.PercentOutput, left_speed * Constants.DRIVE_CONTROL);
    rightMotorMaster.set(ControlMode.PercentOutput, right_speed * Constants.DRIVE_CONTROL);

    SmartDashboard.putNumber("left encoder", leftMotorMaster.getSelectedSensorPosition()*Constants.TICKS_TO_CENTIMETERS);
    SmartDashboard.putNumber("right encoder", rightMotorMaster.getSelectedSensorPosition()*Constants.TICKS_TO_CENTIMETERS);
    SmartDashboard.putNumber("right encoder speed", rightMotorMaster.getSelectedSensorVelocity());
    SmartDashboard.putNumber("left encoder speed", leftMotorMaster.getSelectedSensorVelocity());


  }
  public void setEncoders(){
    rightMotorMaster.setSelectedSensorPosition(0,0,10);
    leftMotorMaster.setSelectedSensorPosition(0,0,10);
  }
  
  public double getGyroAngle() {
    SmartDashboard.putNumber("gyro angle", gyro.getGyroAngleZ());
     return gyro.getGyroAngleZ();
  }

  public void stopDrive() {
    setMotors(0, 0);
  }

  public void resetGyro() {
     gyro.reset();
  }

  @Override
  public void periodic() {
  }
}
