/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;


import com.revrobotics.CANSparkMax;

import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ShooterSubsystem extends SubsystemBase {
  /**
   * Creates a new Shooter.
   */
  public ShooterSubsystem() {
    rightMotor1.setInverted(true);
  }

  private CANSparkMax leftMotor1 = new CANSparkMax(Constants.SHOOTER_LEFT_MOTOR, MotorType.kBrushless);
  private CANSparkMax rightMotor1 = new CANSparkMax(Constants.SHOOTER_RIGHT_MOTOR, MotorType.kBrushless);
  private Servo leftAngleServo = new Servo(Constants.LEFT_SERVO_PIN);
  private Servo rightAngleServo = new Servo(Constants.RIGHT_SERVO_PIN);
  private double alpha, v;
  private void parabola(double distance){
    final double g = 9.81;
    final double h = 2.5;
    double vy,vx,t;
    vy = Math.sqrt(2*g*h);
    t = vy/g;
    vx = distance/t;
    v = Math.sqrt(vy*vy + vx*vx);
    alpha = Math.toDegrees(Math.asin(vy/v));    
  }
//sets the motors to the requested speed
  public void SetMotors(double distance) {
    parabola(distance);
    leftMotor1.set(0.5); 
    rightMotor1.set(0.5);
  }

//sets the angle of the shooter
  public void setPosition(double distance){    
      parabola(distance);
      leftAngleServo.setAngle(alpha/1.5);
      rightAngleServo.setAngle((270-alpha)/1.5);
  }

  public void setStarting(double angle){    
    leftAngleServo.setAngle(angle/1.5);
    rightAngleServo.setAngle((270-angle)/1.5);
}

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}

