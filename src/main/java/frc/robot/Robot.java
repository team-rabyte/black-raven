/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.Shooter.commands.SetShooter;
import frc.robot.Shooter.commands.Shoot;
import frc.robot.drivetrain.TeleopDrive;
import frc.robot.subsystems.DriveTrain;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public static Command m_autonomousCommand;
  public static RobotContainer robotContainer;
  public static ShooterSubsystem shooter;
  public static DriveTrain driveTrain;

  @Override
  public void robotInit() {
    robotContainer = new RobotContainer();
    shooter = new ShooterSubsystem();
    driveTrain = new DriveTrain();
  
    //CameraServer.getInstance();
    //camServer.startAutomaticCapture();
  }

  @Override
  public void robotPeriodic() {
    CommandScheduler.getInstance().run();
  }

  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
  }

  @Override
  public void autonomousInit() {
  }

  @Override
  public void autonomousPeriodic() {
  }

  @Override
  public void teleopInit() {
    // CommandScheduler.getInstance().schedule(new SetShooter());
    CommandScheduler.getInstance().setDefaultCommand(driveTrain, new TeleopDrive());
    //CommandScheduler.getInstance().schedule(new Shoot());
   // motor.set(0.2);
   // motor1.set(-0.2);
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
  }

  @Override
  public void teleopPeriodic() {
    CommandScheduler.getInstance().run();
  }

  @Override
  public void testInit() {
    CommandScheduler.getInstance().cancelAll();
  }

  @Override
  public void testPeriodic() {
    CommandScheduler.getInstance().run();
  }
}
