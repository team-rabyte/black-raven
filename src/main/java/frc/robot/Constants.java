/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   FES*/
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {


	public static final int SHOOTER_RIGHT_MOTOR = 4;
	public static final int SHOOTER_LEFT_MOTOR = 5
	;

	public static final int LEFT_SERVO_PIN = 8;
	public static final int RIGHT_SERVO_PIN = 9;

	public static final int DRIVE_BASE_LEFT_MOTOR_MASTER = 1;
	public static final int DRIVE_BASE_LEFT_MOTOR_SLAVE = 2;
	public static final int DRIVE_BASE_RIGHT_MASTER = 3;
	public static final int DRIVE_BASE_RIGHT_MOTOR_SLAVE = 4;

	public static final double DRIVE_CONTROL = 0.4;

	public static final double PAD_DEADBOUND = 0.1;

	public static final double POWER_PORT_INACCURACY = 0.1;

	// Drive base PID constants
	public static final double DRIVE_BASE_Kp = 0.02;
	public static final double DRIVE_BASE_Ki = 0.01;
	public static final double DRIVE_BASE_Kd = 0.0;
	public static final double DRIVE_BASE_MIN_I = 0.001;
	public static final double DRIVE_BASE_MAX_I = 0.1;
	public static final double DRIVE_BASE_PID_TOLERANCE = 3;

	public static final int LX_AXIS = 0;
	public static final int LY_AXIS = 1;
	public static final int RX_AXIS = 4;
	public static final int RY_AXIS = 5;
	public static final int L_TRIGGER = 2;
	public static final int R_TRIGGER = 3;
	public static final int Y_BUTTON = 3;
	public static final int PAD_PORT = 0;
	
	public static final double KC_VALUE = 0;
	public static final double KV_VALUE = 0;
	public static final double TRACK_WIDTH = 0.555517;

	public static final double TICKS_TO_CENTIMETERS = 0.004349359699;

}


